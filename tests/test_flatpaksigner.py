# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import re

import pytest

from sftpnotary import flatpaksigner

goodBundleHeader = (
    b"flatpak\0\x42\x00\x89\xe5\x00\x75\x08\x00ref\0\0\0\0\0app/org.example.abcde/x86_64/master\0\x00\x73\x04\x00"
)
goodBundleHeaderBigEndian = (
    b"flatpak\0\x42\x00\x89\xe5\x00\x75\x08\x00ref\0\0\0\0\0app/org.example.abcde/x86_64/master\0\x00\x73\x04\x00"
)

truncatedBundleHeader = b"flatpak\0\x01\x00\x89\xe5\x00\x75\x08\x00"
invalidBundleHeader1 = (
    b"flaptak\0\x01\x00\x89\xe5\x00\x75\x08\x00ref\0\0\0\0\0app/org.example.abcde/x86_64/master\0\x00\x73\x04\x00"
)
invalidBundleHeader2 = (
    b"flatpak\0\x01\x00\x98\xe5\x00\x75\x08\x00ref\0\0\0\0\0app/org.example.abcde/x86_64/master\0\x00\x73\x04\x00"
)
noRefInBundleHeader = (
    b"flatpak\0\x01\x00\x89\xe5\x00\x75\x08\x00fer\0\0\0\0\0app/org.example.abcde/x86_64/master\0\x00\x73\x04\x00"
)
noEndOfRefBundleHeader = b"flatpak\0\x42\x00\x89\xe5\x00\x75\x08\x00ref\0\0\0\0\0app/org.example.abcde/x86_64/master"


class TestFlatpakSigner:
    def test_getRefFromFlatpakBundleHeader(self):
        ref = flatpaksigner.getRefFromFlatpakBundleHeader(goodBundleHeader)
        assert ref == "app/org.example.abcde/x86_64/master"
        ref = flatpaksigner.getRefFromFlatpakBundleHeader(goodBundleHeaderBigEndian)
        assert ref == "app/org.example.abcde/x86_64/master"

    def test_getRefFromFlatpakBundleHeader_errors(self):
        with pytest.raises(Exception, match=re.escape("Truncated or invalid Flatpak bundle (smaller than 32 bytes)")):
            flatpaksigner.getRefFromFlatpakBundleHeader(truncatedBundleHeader)
        with pytest.raises(Exception, match=re.escape("Invalid Flatpak bundle header (doesn't start with 'flatpak')")):
            flatpaksigner.getRefFromFlatpakBundleHeader(invalidBundleHeader1)
        with pytest.raises(Exception, match=re.escape("Invalid Flatpak bundle header (wrong magic number)")):
            flatpaksigner.getRefFromFlatpakBundleHeader(invalidBundleHeader2)
        with pytest.raises(Exception, match=re.escape("Start of 'ref' entry not found")):
            flatpaksigner.getRefFromFlatpakBundleHeader(noRefInBundleHeader)
        with pytest.raises(Exception, match=re.escape("End of 'ref' value not found")):
            flatpaksigner.getRefFromFlatpakBundleHeader(noEndOfRefBundleHeader)
