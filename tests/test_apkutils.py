# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import pytest

from sftpnotary import apkutils


class TestApkutils:
    def test_filterFastlaneFileNames(self):
        assert apkutils.filterFastlaneFileNames(["org.example.foo.yml"], "org.example.foo") == ["org.example.foo.yml"]

        # allowed images (only featureGraphic.png and icon.png)
        assert apkutils.filterFastlaneFileNames(
            ["org.example.foo/en-US/images/featureGraphic.png"], "org.example.foo"
        ) == ["org.example.foo/en-US/images/featureGraphic.png"]
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/images/icon.png"], "org.example.foo") == [
            "org.example.foo/en-US/images/icon.png"
        ]

        # allowed phoneScreenshots (any *.png files)
        assert apkutils.filterFastlaneFileNames(
            ["org.example.foo/en-US/images/phoneScreenshots/foo.png"], "org.example.foo"
        ) == ["org.example.foo/en-US/images/phoneScreenshots/foo.png"]

        # allowed text files (only full_description.txt, short_description.txt, and title.txt)
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/full_description.txt"], "org.example.foo") == [
            "org.example.foo/en-US/full_description.txt"
        ]
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/short_description.txt"], "org.example.foo") == [
            "org.example.foo/en-US/short_description.txt"
        ]
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/title.txt"], "org.example.foo") == [
            "org.example.foo/en-US/title.txt"
        ]

        # names with two letter language code (without country code)
        assert apkutils.filterFastlaneFileNames(["org.example.foo/xx/images/icon.png"], "org.example.foo") == [
            "org.example.foo/xx/images/icon.png"
        ]
        assert apkutils.filterFastlaneFileNames(
            ["org.example.foo/xx/images/phoneScreenshots/foo.png"], "org.example.foo"
        ) == ["org.example.foo/xx/images/phoneScreenshots/foo.png"]
        assert apkutils.filterFastlaneFileNames(["org.example.foo/xx/full_description.txt"], "org.example.foo") == [
            "org.example.foo/xx/full_description.txt"
        ]

    def test_filterFastlaneFileNames_unexpected_names(self):
        # the full file paths must match
        assert apkutils.filterFastlaneFileNames(["org.example.foo.ymlx"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["xorg.example.foo.yml"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/images/icon.pngx"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["xorg.example.foo/en-US/images/icon.png"], "org.example.foo") == []
        assert (
            apkutils.filterFastlaneFileNames(
                ["org.example.foo/en-US/images/phoneScreenshots/foo.pngx"], "org.example.foo"
            )
            == []
        )
        assert (
            apkutils.filterFastlaneFileNames(
                ["xorg.example.foo/en-US/images/phoneScreenshots/foo.png"], "org.example.foo"
            )
            == []
        )
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/foo.txtx"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["xorg.example.foo/en-US/foo.txt"], "org.example.foo") == []

        # folder must match application ID
        assert apkutils.filterFastlaneFileNames(["org.example.bar/en-US/images/icon.png"], "org.example.foo") == []
        assert (
            apkutils.filterFastlaneFileNames(
                ["org.example.bar/en-US/images/phoneScreenshots/foo.png"], "org.example.foo"
            )
            == []
        )
        assert apkutils.filterFastlaneFileNames(["org.example.bar/en-US/foo.txt"], "org.example.foo") == []

        # invalid language/country codes
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en_US/images/icon.png"], "org.example.foo") == []
        assert (
            apkutils.filterFastlaneFileNames(
                ["org.example.foo/en_US/images/phoneScreenshots/foo.png"], "org.example.foo"
            )
            == []
        )
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en_US/foo.txt"], "org.example.foo") == []

        # disallowed file names
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en_US/images/foo.png"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en_US/images/icon.exe"], "org.example.foo") == []
        assert (
            apkutils.filterFastlaneFileNames(
                ["org.example.foo/en_US/images/phoneScreenshots/foo.exe"], "org.example.foo"
            )
            == []
        )
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en-US/foo.txt"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["org.example.foo/en_US/title.py"], "org.example.foo") == []

    def test_filterFastlaneFileNames_dangerous_names(self):
        assert apkutils.filterFastlaneFileNames(["/etc/passwd"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["../../etc/passwd"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["foo/../../etc/passwd"], "org.example.foo") == []
        assert apkutils.filterFastlaneFileNames(["org.example.foo/bla..txt"], "org.example.foo") == []

    def test_filterFastlaneFileNames_invalid_applicationId(self):
        with pytest.raises(ValueError):
            apkutils.filterFastlaneFileNames([], "")
        with pytest.raises(ValueError):
            apkutils.filterFastlaneFileNames([], "/org.example.foo")
        with pytest.raises(ValueError):
            apkutils.filterFastlaneFileNames([], "org/example.foo")
