# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import logging
import re

from sftpnotary import config, util
from sftpnotary.exceptions import Error

log = logging.getLogger(__name__)


def getApplicationId(apkFilePath):
    workingDirectory = apkFilePath.parent
    apkFileName = apkFilePath.name
    apkAnalyzerPath = config.settings.getPath("ApkSigning", "APKAnalyzerPath")
    command = [apkAnalyzerPath, "manifest", "application-id", apkFileName]
    result = util.runCommand(command, cwd=workingDirectory)
    if not result.stdout:
        raise Error("apkanalyzer did not output an application ID")
    return result.stdout.splitlines()[0].decode(errors="replace")


def filterFastlaneFileNames(names, applicationId):
    """Removes any unexpected file names from the list of files in the Fastlane zip file.

    In particular, all folder names (ending with '/') are removed. But that's okay
    because when extracting files in those folders the folders will be created anyway.

    names should be the return value of ZipFile.namelist().
    """

    if not applicationId or "/" in applicationId:
        raise ValueError("Invalid application ID: Empty or contains '/'")

    # first filter out obviously bad file paths: absolute paths and any
    # file paths containing ".." (even inside a file name or folder name);
    # this shouldn't be necessary because those paths should never match
    # one of the expected names below, but better safe than sorry
    filteredNames = [name for name in names if not name.startswith("/") and ".." not in name]

    # then filter out everything not matching the expected names
    escapedAppId = re.escape(applicationId)
    # regular expression for locale IDs (without extension) supported by Android; see
    # https://android.googlesource.com/platform/frameworks/base/+/master/core/res/res/values/locale_config.xml
    # additionally, for some locales the language code (without country code, etc.) also works;
    # numeric region codes are not allowed because the only locale ID with such a code (ia-001) is not
    # accepted by the Google Play API
    localeID = (
        "(?:[a-z]{2,3})"  # language
        "(?:-Adlm|-Arab|-Cyrl|-Deva|-Guru|-Hans|-Hant|-Latn)?"  # script (optional)
        "(?:-[A-Z]{2})?"  # region (optional)
    )
    notEmptyStarGlob = "[^/]+"
    expectedNamesREs = [
        # <application ID>.yml
        re.compile(f"{escapedAppId}\\.yml"),
        # files in <application ID>/<language code> folders:
        # images/featureGraphic.png, images/icon.png
        re.compile(f"{escapedAppId}/{localeID}/images/featureGraphic\\.png"),
        re.compile(f"{escapedAppId}/{localeID}/images/icon\\.png"),
        # images/phoneScreenshots/*.png
        re.compile(f"{escapedAppId}/{localeID}/images/phoneScreenshots/{notEmptyStarGlob}\\.png"),
        # full_description.txt, short_description.txt, title.txt
        re.compile(f"{escapedAppId}/{localeID}/full_description\\.txt"),
        re.compile(f"{escapedAppId}/{localeID}/short_description\\.txt"),
        re.compile(f"{escapedAppId}/{localeID}/title\\.txt"),
    ]
    filteredNames = [name for name in filteredNames if any(regex.fullmatch(name) for regex in expectedNamesREs)]
    unexpectedNames = sorted(set(names) - set(filteredNames))
    unexpectedNames = [n for n in unexpectedNames if not n.endswith("/")]  # remove folder names
    if unexpectedNames:
        log.warning(f"Found unexpected files in the Fastlane zip file: {unexpectedNames}")

    return filteredNames
