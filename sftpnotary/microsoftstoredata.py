# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import dataclasses
from typing import Optional

from sftpnotary import util
from sftpnotary.core import Request, Response


@dataclasses.dataclass
class PublishOnMicrosoftStoreRequest(Request):
    fileName: Optional[str] = None

    def validate(self):
        util.validateFileName(self.fileName, strict=True)


PublishOnMicrosoftStoreResponse = Response
