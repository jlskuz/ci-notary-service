# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2021 Volker Krause <vkrause@kde.org>
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import dataclasses
import logging
import os
import re
import zipfile
from pathlib import Path
from tempfile import TemporaryDirectory
from typing import List, Optional

from sftpnotary import apkutils, config, projects, util
from sftpnotary.core import Job, Request, Response, TaskProcessor, Worker
from sftpnotary.exceptions import Error, InvalidFileName
from sftpnotary.sftp import SFTPClient, SFTPError

log = logging.getLogger(__name__)


@dataclasses.dataclass
class PublishOnGooglePlayRequest(Request):
    apkFileNames: Optional[List[str]] = None
    fastlaneFileName: Optional[str] = None

    def validate(self):
        if self.apkFileNames is None:
            raise InvalidFileName()
        for apkFileName in self.apkFileNames:
            util.validateFileName(apkFileName, strict=True)
        util.validateFileName(self.fastlaneFileName, strict=True)


PublishOnGooglePlayResponse = Response


class PublishOnGooglePlayJob(Job):
    def __init__(
        self,
        sftp: SFTPClient,
        apkFilePaths: List[Path],
        fastlaneFilePath: Path,
        token: Optional[str] = None,
        projectPath: Optional[str] = None,
        branch: Optional[str] = None,
    ):
        super().__init__(sftp)

        self.apkFilePaths = apkFilePaths
        self.fastlaneFilePath = fastlaneFilePath
        self.request = PublishOnGooglePlayRequest(
            apkFileNames=[],
            fastlaneFileName=None,
            token=token,
            projectPath=projectPath,
            branch=branch,
        )
        self.resultPath = self.apkFilePaths[0].parent

    def start(self):
        for apkFilePath in self.apkFilePaths:
            apkFileName = apkFilePath.name
            self.sftp.upload(apkFilePath, self.task.path() / apkFileName)
            self.request.apkFileNames.append(apkFileName)
        fastlaneFileName = self.fastlaneFilePath.name
        self.sftp.upload(self.fastlaneFilePath, self.task.path() / fastlaneFileName)
        self.request.fastlaneFileName = fastlaneFileName
        self.task.putRequest(self.request)
        self.taskQueue.commitTask(self.task)

    def waitForCompletion(self):
        self.taskQueue.waitForCompletion(self.task)
        log.debug(f"Fetching response of task '{self.task.id}' ...")
        response = None
        try:
            response = self.task.fetchResponse(PublishOnGooglePlayResponse)
        except SFTPError as e:
            log.error("Error: %s", e)
        if response is None:
            log.error("Error: Got no response. Task failed.")
        else:
            self.success = True
        self.downloadLogFiles(self.resultPath)
        self.taskQueue.removeTask(self.task)


GEM_FILE_TEMPLATE = """
source "https://rubygems.org"
gem "fastlane"
"""

APP_FILE_TEMPLATE = """
json_key_file("{credentialsFilePath}")
package_name("{applicationId}")
"""

FAST_FILE_TEMPLATE = """
lane :upload_metadata do
   google_version_code = google_play_track_version_codes(track: 'beta')[0]
   supply(
      package_name: "{applicationId}",
      track: "beta",
      skip_upload_apk: true,
      skip_upload_aab: true,
      skip_upload_changelogs: true,
      skip_upload_metadata: false,
      skip_upload_images: {skipImages},
      skip_upload_screenshots: {skipImages},
      version_code: google_version_code
    )
end
"""


class PublishOnGooglePlayProcessor(TaskProcessor):
    requestClass = PublishOnGooglePlayRequest

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fastlaneUpdatePath = config.settings.getPath("GooglePlayPublishing", "FastlaneDirectory")
        self.fastlaneWorkPath = config.settings.getPath("GooglePlayPublishing", "FastlaneWorkingDirectory")
        self.credentialsFilePath = config.settings.getPath("GooglePlayPublishing", "Credentials")
        self.bundleExecutable = config.settings.getPath("GooglePlayPublishing", "BundleExecutable")
        self.bundlePath = config.settings.getPath("GooglePlayPublishing", "BundlePath")
        self.bundleAppConfig = config.settings.getPath("GooglePlayPublishing", "BundleAppConfig")

        self.applicationId = ""

    def getProjectSetting(self, name, required=True):
        value = projects.settings.get(self.request.projectPath, self.request.branch, name)
        if required and not value:
            raise Error(
                f"Project setting '{name}' not set for branch {self.request.branch} "
                f"of project {self.request.projectPath}"
            )
        return value

    def doProcess(self):
        self.updateFastlane()

        with TemporaryDirectory() as tempPathName:
            tempPath = Path(tempPathName)

            localApkFilePaths = []
            for apkFileName in self.request.apkFileNames:
                localApkFilePath = tempPath / apkFileName
                self.sftp.download(self.task.path() / apkFileName, localApkFilePath)
                localApkFilePaths.append(localApkFilePath)
            localFastlaneFilePath = tempPath / self.request.fastlaneFileName
            self.sftp.download(self.task.path() / self.request.fastlaneFileName, localFastlaneFilePath)

            allowedApplicationId = projects.settings.get(self.request.projectPath, self.request.branch, "applicationid")
            self.verifyApplicationId(localApkFilePaths, allowedApplicationId)
            self.applicationId = allowedApplicationId
            self.publishApks(localApkFilePaths, localFastlaneFilePath)

    def updateFastlane(self):
        command = [self.bundleExecutable, "update", "fastlane"]
        workPath = self.fastlaneUpdatePath
        environment = {
            **os.environ,
            "BUNDLE_EXECUTABLE": self.bundleExecutable,
            "BUNDLE_PATH": self.bundlePath,
            "BUNDLE_APP_CONFIG": self.bundleAppConfig,
        }
        try:
            util.runCommand(command, cwd=workPath, env=environment)
        except Error:
            # continue the publishing process even if the update of Fastlane failed
            pass

    def verifyApplicationId(self, apkFilePaths, allowedApplicationId):
        for apkFilePath in apkFilePaths:
            applicationId = apkutils.getApplicationId(apkFilePath)
            if applicationId != allowedApplicationId:
                raise Error(
                    f"The application ID of the APK ({applicationId}) did not match the allowed application ID "
                    f"({allowedApplicationId})."
                )

    def publishApks(self, apkFilePaths, fastlaneFilePath):
        #
        # Configure Fastlane for this app
        #
        workPath = self.fastlaneWorkPath / self.applicationId
        os.makedirs(workPath / "fastlane", exist_ok=True)

        # write Gemfile
        with open(workPath / "Gemfile", "w") as f:
            f.write(GEM_FILE_TEMPLATE)
        # remove Gemfile.lock, otherwise the shared updates won't be visible in this remains
        # pinned to the first fastlane version it ever saw
        lockfile = workPath / "Gemfile.lock"
        lockfile.unlink(missing_ok=True)

        # write Appfile
        with open(workPath / "fastlane" / "Appfile", "w") as f:
            f.write(
                APP_FILE_TEMPLATE.format(credentialsFilePath=self.credentialsFilePath, applicationId=self.applicationId)
            )

        self.updateMetaData(fastlaneFilePath)

        #
        # Upload APKs
        #
        log.info("Uploading APKs")
        self.runFastlaneCommand(
            [
                "supply",
                "--apk_paths",
                ",".join(str(p) for p in apkFilePaths),
                "--track",
                "beta",
                "--release_status",
                "draft",
                "--skip_upload_images",
                "true",
                "--skip_upload_screenshots",
                "true",
            ]
        )

    def updateMetaData(self, fastlaneFilePath):
        workPath = self.fastlaneWorkPath / self.applicationId

        #
        # Download existing meta data for comparison
        #
        log.info("Downloading existing metadata")
        self.runFastlaneCommand(["supply", "init"])

        #
        # check whether there are metadata updates
        #
        textChanged = False
        imageChanged = False
        with zipfile.ZipFile(fastlaneFilePath, "r") as zipFile:
            fileNames = apkutils.filterFastlaneFileNames(zipFile.namelist(), self.applicationId)
            for fileName in fileNames:
                # directories or top-level elements
                if "/" not in fileName or fileName.endswith("/"):
                    continue

                # determine the file name in the fastlane structure (without trailing <applicationId> folder)
                _, relativeFileName = fileName.split("/", maxsplit=1)
                outFileName = workPath / "fastlane" / "metadata" / "android" / relativeFileName
                # adjust file names for screenshots
                outFileName = Path(
                    re.sub(
                        r"/android/([^/]*)/images/([^/]*)/(\d)-.*\.png",
                        r"/android/\1/images/\2/\3_\1.png",
                        str(outFileName),
                    )
                )

                with zipFile.open(fileName) as inFile:
                    inData = inFile.read()

                outData = b""
                if outFileName.exists():
                    with open(outFileName, "rb") as outFile:
                        outData = outFile.read()

                if inData == outData:
                    log.debug(f"comparing '{fileName}' and '{outFileName}'... no change")
                    continue

                log.debug(f"comparing '{fileName}' and '{outFileName}'... needs updating")
                outFileName.parent.mkdir(parents=True, exist_ok=True)
                with open(outFileName, "wb") as outFile:
                    outFile.write(inData)

                # remember what we need to update
                if outFileName.suffix == ".png":
                    imageChanged = True
                else:
                    textChanged = True

        #
        # Upload metadata changes if necessary
        #
        if not textChanged and not imageChanged:
            log.info("Skipping upload of metadata")
            log.debug("No metadata changes found.")
        else:
            log.info("Uploading metadata")
            log.debug(f"Needs image asset upload: {imageChanged}")
            # write Fastfile
            with open(workPath / "fastlane" / "Fastfile", "w") as f:
                f.write(
                    FAST_FILE_TEMPLATE.format(
                        applicationId=self.applicationId, skipImages=("false" if imageChanged else "true")
                    )
                )
            # submit updates
            self.runFastlaneCommand(["upload_metadata"])

    def runFastlaneCommand(self, command):
        command = [self.bundleExecutable, "exec", "fastlane"] + command
        workPath = self.fastlaneWorkPath / self.applicationId
        environment = {
            **os.environ,
            "BUNDLE_EXECUTABLE": self.bundleExecutable,
            "BUNDLE_PATH": self.bundlePath,
            "BUNDLE_APP_CONFIG": self.bundleAppConfig,
        }
        util.runCommand(command, cwd=workPath, env=environment)


class PublishOnGooglePlayWorker(Worker):
    processorClass = PublishOnGooglePlayProcessor
