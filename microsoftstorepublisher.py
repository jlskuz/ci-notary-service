#!/usr/bin/env python3
# SPDX-License-Identifier: BSD-2-Clause
#
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>

import logging
import sys

from sftpnotary import authentication, config, util
from sftpnotary.exceptions import Error
from sftpnotary.microsoftstoreservice import PublishOnMicrosoftStoreWorker


def parseCommandLine():
    import argparse

    parser = argparse.ArgumentParser(description="Worker for submitting APPXs to Microsoft Store")

    # debug options
    parser.add_argument("-v", "--verbose", action="count", default=0, help="increase the verbosity")
    parser.add_argument(
        "--debug-authorization",
        type=int,
        default=0,
        dest="debugAuth",
        metavar="LEVEL",
        help="debug level for authorization to Microsoft Store (1 or 2)",
    )
    parser.add_argument(
        "--debug-api-calls",
        type=int,
        default=0,
        dest="debugApi",
        metavar="LEVEL",
        help="debug level for API calls to Microsoft Store (1 or 2)",
    )

    parser.add_argument("--id", dest="workerId", help="Unique, alpha-numeric identifier of this service worker")

    parser.add_argument("--config")

    options = parser.parse_args()
    return options


def setUpAuthentication():
    auth = authentication.GitLabJobTokenAuthentication(
        apiV4Url=config.settings.get("Authentication", "GitLabAPIv4Url", ""),
        userAgent="org.kde.microsoftstorepublisher",
    )
    authentication.setAuthentication(auth)


def startWorker(workerId: str):
    with util.connectToSftpServer() as sftp:
        worker = PublishOnMicrosoftStoreWorker(workerId, sftp)
        worker.start()


def main():
    options = parseCommandLine()
    config.loadConfig(options.config)
    util.setUpServiceLogging("microsoftstorepublisher", options.verbose)
    microstoreLogger = logging.getLogger("microstore")
    microstoreLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.verbose))
    authLogger = logging.getLogger("microstore.auth")
    authLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugAuth))
    apiLogger = logging.getLogger("microstore.api")
    apiLogger.setLevel(max(logging.DEBUG, logging.WARNING - 10 * options.debugApi))

    util.loadProjectSettings(config.settings.get("General", "ProjectSettings"))
    if config.settings.getboolean("Authentication", "Enabled", False):
        setUpAuthentication()

    startWorker(options.workerId)

    return 0


if __name__ == "__main__":
    try:
        sys.exit(main())
    except Error as e:
        print("Error: %s" % e, file=sys.stderr)
        sys.exit(1)
