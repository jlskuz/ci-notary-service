<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2024 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# windowsbinariessigner.py - A Signing Service for Windows Binaries and APPX Packages

Provides code signing for Windows binaries and APPX sideload packages.

## Prerequisites

You need
* Python 3.8+ (I entered `python` in PowerShell and then the Microsoft Store app opened offering Python 3.11)
* Git (install with `winget install --id Git.Git -e --source winget`; then open a new PowerShell)
* a code signing certificate (either as `*.pfx` file or in your certificate store),
* a config file (see `windowsbinariessigner.sample.ini`),
* a project settings file (see `windowsbinariessigner-projects.sample.yaml`),
* a file with the private SSH key, e.g. `testsshkey`, without password for
  accessing the SFTP server.

Moreover, you need a directory (e.g. `windowsbinariessigner`) on an SFTP server that can be used for
exchanging data with the clients.

## Installation

### Install the Service

Clone this repository on a Windows machine that shall provide the service,
create a virtual environment, and install the requirements.

In PowerShell do
```powershell
git clone https://invent.kde.org/sysadmin/ci-notary-service.git windowsbinariessigner
cd .\windowsbinariessigner
python -m venv $env:USERPROFILE\venv\windowsbinariessigner
~\venv\windowsbinariessigner\Scripts\Activate.ps1
pip3 install -r requirements.txt
```

#### Note

> It may be necessary to set the execution policy for the user to be able to run the `Activate.ps1` script. You can do this by issuing the following PowerShell command:
> `Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Scope CurrentUser`
> See [About Execution Policies](https://go.microsoft.com/fwlink/?LinkID=135170) for more information.

### Configure the Service

Copy `windowsbinariessigner.sample.ini` to `windowsbinariessigner.ini` and replace the example values
with the appropriate values for your installation.

Then copy `windowsbinariessigner-projects.sample.yaml` to `windowsbinariessigner-projects.yaml` and
specify the settings for the projects you want to sign Windows binaries or APPX sideload packages for.
If you want to sign APPX sideload packages then you have to add the application IDs of the projects
to the project settings. The application ID of a project is the value of the `Name` attribute of the
`Identity` element in the `AppxManifest.xml`.

## Run the Service

Enter the folder containing `windowsbinariessigner.py`, install the needed Python packages, and run the script
```powershell
cd .\windowsbinariessigner
~\venv\windowsbinariessigner\Scripts\Activate.ps1
python windowsbinariessigner.py --config windowsbinariessigner.ini --id workerA
```

## Use the Service to Sign Windows Binaries

Install the necessary Python modules in a virtual environment (e.g. on Linux)

```sh
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

Copy `signwindowsbinaries.sample.ini` to `signwindowsbinaries.ini` and replace the example values
with the appropriate values for your installation. In particular, the value
for `BasePath` must match the corresponding value in the settings of the
service.

Then run `signwindowsbinaries.py` to sign Windows binaries

```sh
python3 signwindowsbinaries.py --config signwindowsbinaries.ini <binary> ...
```

or

```sh
python3 signwindowsbinaries.py --config signwindowsbinaries.ini --files-from <file>
```

where `<file>` contains a list of files to sign. Use the later command if you want to sign a lot of files
and want to avoid problems caused by command line length limitations.

## Creating a Test Certificate

See https://learn.microsoft.com/en-us/windows/msix/package/create-certificate-package-signing
