<!--
# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 KDE e.V.
# SPDX-FileContributor: Ingo Klöcker <dev@ingo-kloecker.de>
-->

# Technical Details

## Task Workflow

For each kind of task, e.g. signing of APKs, signing of APPXs, etc., a base folder
exists on an SFTP server. The base folders must be created manually.
Each base folder contains the following subfolders

* `tmp`
* `queue`
* `processing-<worker ID>` for each worker where `worker ID` is a unique alpha-numeric ID
* `done`
* `trash`

For each task a folder containing the needed data and meta-data moves from one of those
stage folders to the next one. The naming scheme for the task folders is
`<simplified ISO timestamp>-<client ID>`, e.g. 20230516T132050-invent. Currently,
the hostname is used as client ID.

### Task Stages

#### Creating a new task

Clients create a new task folder in `tmp` and upload all necessary data to the
task folder. Then they commit the task by moving it from `tmp` to `queue`.

#### Processing the task queue

One or more workers watch (poll) `queue` for new task folders. When they see a task folder,
they try to take it by moving it to their `processing-*` folder. If they successfully
took the task, they process the task, i.e. they download the data, process the
data (e.g. by signing it), and upload the results. Then they set the task to done
by moving it from their `processing-*` folder to `done`.

#### Retrieving the results

The client watches (polls) the `done` folder for its task folder. When the task
folder is found, they download the results. Then they remove the task which
currently moves the task folder from `done` to `trash`.
